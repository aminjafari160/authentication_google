import 'package:flutter/material.dart';
import 'authentication.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Authentication _authentication = Authentication();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool isSign = false;
  AuthBloc _authBloc = AuthBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: isSign
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                    child: _authentication.user.photoUrl != null
                        ? Image.network(_authentication.user.photoUrl)
                        : SizedBox(),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Email",
                  ),
                  Text(
                    "${_authentication.user.email}",
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Name",
                  ),
                  Text(
                    "${_authentication.user.displayName}",
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(labelText: "Email"),
                    controller: _email,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "Password"),
                    controller: _password,
                  ),
                  InkWell(
                    onTap: () {
                      _authBloc.loginFacebook();
                    },
                    child: Container(
                      color: Colors.red,
                      width: 60,
                      height: 60,
                      child: Text("login with facebook"),
                    ),
                  ),
                ],
              ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          FloatingActionButton(
            heroTag: "1",
            onPressed: () {
              _authentication.signOutGoogle();
              setState(() {});
              isSign = false;
            },
            tooltip: 'Increment',
            child: Text(
              "Sign out",
              style: TextStyle(fontSize: 15),
            ),
          ),
          FloatingActionButton(
            heroTag: "1",
            onPressed: () {
              _authentication.signInWithGoogle().then((value) {
                setState(() {
                  isSign = true;
                });
              });
            },
            tooltip: 'Increment',
            child: Text("Sign in"),
          ),
          FloatingActionButton(
            heroTag: "1",
            onPressed: () {
              _authentication
                  .signInWithEmail(_email.text, _password.text)
                  .then((value) {
                setState(() {
                  isSign = true;
                });
              });
            },
            tooltip: 'Increment',
            child: Text("Sign in Email"),
          ),
          FloatingActionButton(
            heroTag: "1",
            onPressed: () {
              _authentication
                  .resetPassword("aminjafari160@gmail.com")
                  .then((value) {
                print(value);
                // setState(() {
                //   isSign = true;
                // });
              });
            },
            tooltip: 'Increment',
            child: Text("Reset Password"),
          ),
        ],
      ),
    );
  }
}
