import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Authentication {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  FirebaseUser user;

  // sign in with google
  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    user = authResult.user;
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    print('signInWithGoogle succeeded: ${user.email}');
    return 'signInWithGoogle succeeded: ${user.email}';
  }

  // sign in with google
  Future<String> signInWithFacebook() async {
    
    var a = await signInWithFacebook();
    print(a);
  }

  // register with Email
  Future registerWithEmail(String email, String pass) async {
    try {
      AuthResult _authResult = await _auth.createUserWithEmailAndPassword(
          email: email, password: pass);
      user = _authResult.user;
      return user;
    } catch (e) {
      print(e);
    }
  }

  // sign in with Email
  Future signInWithEmail(String email, String pass) async {
    try {
      AuthResult _authResult =
          await _auth.signInWithEmailAndPassword(email: email, password: pass);
      user = _authResult.user;
      return user;
    } catch (e) {
      print(e);
    }
  }

  // sign in with Email
  Future resetPassword(String email) async {
    try {
      var amin = await _auth.sendPasswordResetEmail(email: email);
      return amin;
    } catch (e) {
      print(e);
    }
  }

  // sign in with phone
  Future signInWithPhone(String email, String pass) async {
    try {
      // AuthResult _authResult = await _auth.verifyPhoneNumber(phoneNumber: null, timeout: null, verificationCompleted: null, verificationFailed: null, codeSent: null, codeAutoRetrievalTimeout: null));
      // user = _authResult.user;
      return user;
    } catch (e) {
      print(e);
    }
  }

  // signaout

  void signOutGoogle() async {
    await googleSignIn.signOut();

    print("User Sign Out");
  }
}

// class AuthBloc {
//   final authService = AuthService();
//   final fb = FacebookLogin();

//   Stream<FirebaseUser> get currentUser => authService.currentUser;

//   loginFacebook() async {
//     print('Starting Facebook Login');

//     final res = await fb.logIn(permissions: [
//       FacebookPermission.publicProfile,
//       FacebookPermission.email
//     ]);

//     switch (res.status) {
//       case FacebookLoginStatus.Success:
//         print('It worked');

//         //Get Token
//         final FacebookAccessToken fbToken = res.accessToken;

//         //Convert to Auth Credential
//         final AuthCredential credential =
//             FacebookAuthProvider.getCredential(accessToken: fbToken.token);

//         //User Credential to Sign in with Firebase
//         final result = await authService.signInWithCredentail(credential);

//         print('${result.user.displayName} is now logged in');

//         break;
//       case FacebookLoginStatus.Cancel:
//         print('The user canceled the login');
//         break;
//       case FacebookLoginStatus.Error:
//         print('There was an error');
//         break;
//     }
//   }

//   logout() {
//     authService.logout();
//   }
// }
class AuthBloc {
  final authService = AuthService();
  final fb = FacebookLogin();

  Stream<FirebaseUser> get currentUser => authService.currentUser;

  loginFacebook() async {
    print('Starting Facebook Login');

    final res = await fb.logIn(
      permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email
      ]
    );

    switch(res.status){
      case FacebookLoginStatus.Success:
      print('It worked');

      //Get Token
      final FacebookAccessToken fbToken = res.accessToken;

      //Convert to Auth Credential
      final AuthCredential credential 
        = FacebookAuthProvider.getCredential(accessToken: fbToken.token);

      //User Credential to Sign in with Firebase
      final result = await authService.signInWithCredentail(credential);

      print('${result.user.displayName} is now logged in');

      break;
      case FacebookLoginStatus.Cancel:
      print('The user canceled the login');
      break;
      case FacebookLoginStatus.Error:
      print('There was an error');
      break;
    }
  }

  logout(){
    authService.logout();
  }
}


class AuthService {
  final _auth = FirebaseAuth.instance;

  Stream<FirebaseUser> get currentUser => _auth.onAuthStateChanged;
  Future<AuthResult> signInWithCredentail(AuthCredential credential) => _auth.signInWithCredential(credential);
  Future<void> logout() => _auth.signOut();
}